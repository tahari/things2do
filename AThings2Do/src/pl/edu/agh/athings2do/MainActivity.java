package pl.edu.agh.athings2do;

import pl.edu.agh.athings2do.R;

import com.aware.Aware;
import com.aware.Aware_Preferences;

import android.app.Activity;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class MainActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		setLocationSensorSettings();
	}

	private void setLocationSensorSettings() {
		//Settings for the sensor
		Aware.setSetting(getContentResolver(), Aware_Preferences.STATUS_LOCATION_GPS, true);
		Aware.setSetting(getContentResolver(), Aware_Preferences.STATUS_LOCATION_NETWORK, true);
		Aware.setSetting(getContentResolver(), Aware_Preferences.FREQUENCY_GPS, 180);
		Aware.setSetting(getContentResolver(), Aware_Preferences.FREQUENCY_NETWORK, 300);
		Aware.setSetting(getContentResolver(), Aware_Preferences.MIN_GPS_ACCURACY, 150);
		Aware.setSetting(getContentResolver(), Aware_Preferences.MIN_NETWORK_ACCURACY, 1500);
		Aware.setSetting(getContentResolver(), Aware_Preferences.EXPIRATION_TIME, 300);
		
		//Apply settings
		Intent applySettings = new Intent(Aware.ACTION_AWARE_REFRESH);
		sendBroadcast(applySettings);
	}

	public void searchPlaces(View v) {
		Intent intent = new Intent(this, SearchPlacesActivity.class);
		startActivity(intent);
	}

	public void editUserProfile(View v) {
		Intent intent = new Intent();
		intent.setComponent(new ComponentName("com.aware.plugin.user_profile", "com.aware.plugin.user_profile.Settings"));
		startActivity(intent);
	}

	//----------------------------------------
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		// deactivate location
		Aware.setSetting(getContentResolver(), Aware_Preferences.STATUS_LOCATION_GPS, false);
		Aware.setSetting(getContentResolver(), Aware_Preferences.STATUS_LOCATION_NETWORK, false);

		//Apply settings
		Intent applySettings = new Intent(Aware.ACTION_AWARE_REFRESH);
		sendBroadcast(applySettings);
		
	}

}
