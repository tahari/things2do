package pl.edu.agh.athings2do.model;

public class WeatherConditions {

	private double temp;
	private double windStr;
	private double rain;
	
	public WeatherConditions(double temp, double windStr, double rain) {
		this.temp = temp;
		this.windStr = windStr;
		this.rain = rain;
	}

	public double getTemp() {
		return temp;
	}

	public double getWindStr() {
		return windStr;
	}

	public double getRain() {
		return rain;
	}

}
