package pl.edu.agh.athings2do.reasoning;

import heart.HeaRT;
import heart.alsvfd.SetValue;
import heart.alsvfd.SimpleNumeric;
import heart.alsvfd.SimpleSymbolic;
import heart.alsvfd.Value;
import heart.exceptions.BuilderException;
import heart.exceptions.NotInTheDomainException;
import heart.parser.hml.HMLParser;
import heart.xtt.State;
import heart.xtt.StateElement;
import heart.xtt.XTTModel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.athings2do.model.WeatherConditions;

import com.aware.plugin.user_profile.model.Category;
import com.aware.plugin.user_profile.model.User;

import android.content.Context;
import android.content.res.AssetManager;

public class PlaceTypesReasoner {
	
	public static String getTypes(Context context, User user, WeatherConditions weather) {
		
		InputStream is;
		AssetManager assetManager = context.getAssets();

	    try {
			is = assetManager.open("at2d_original.hml");
			
			XTTModel model = HMLParser.parseHML(is);
			
			prepareBasicState(model, user, weather);
			
			runRules(model, new String[]{"AGE", "TEMPERATURE", "WIND_STR", "PRECIP", "KIND_OF_SPACE"});
		    
		    // get types by all user interests
		    List<Category> userInterests = getUserInterests(user);
		    for(Category interest: userInterests) {
		    	runRulesForInterest(model, interest);			    	
		    }		    	
		    
		    Value result = model.getAttributeByName("placeType").getValue();
		    
		    return result.toString();
			
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (BuilderException e) {
			e.printStackTrace();
		} catch (NotInTheDomainException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static int typeToCategoryNo(Context context, String type) {
		InputStream is;
		AssetManager assetManager = context.getAssets();

	    try {
			is = assetManager.open("at2d_type_to_cat.hml");
			
			XTTModel model = HMLParser.parseHML(is);
			
			// CREATE STATE ELEMENT
			StateElement typeName = new StateElement();
			typeName.setAttributeName("placeType");						
			// SET VALUE
			typeName.setValue(new SimpleSymbolic(type));
			// ADD BEGINNING STATE TO A MODEL
			State XTTstate = new State();
			LinkedList<StateElement> states = new LinkedList<StateElement>();
			states.add(typeName);
		    XTTstate.setStateElements(states);
			
			model.setCurrentState(XTTstate);

			// RUN RULES
			runRules(model, new String[]{"TYPE_TO_CATEGORY"});
		    
			// GET RESULT
		    Value result = model.getAttributeByName("categoryNo").getValue();
		    
		    return (int) Double.parseDouble(result.toString());
			
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (BuilderException e) {
			e.printStackTrace();
		} catch (NotInTheDomainException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}

	private static void runRulesForInterest(XTTModel model, Category interest)
			throws NotInTheDomainException {
		// set an interest
		model.getAttributeByName("userInterest").setValue(
				new SimpleSymbolic(
						interest.toString()));
		
		// run rules
		runRules(model, new String[]{"CHECK_TYPES"});
	}

	private static List<Category> getUserInterests(User user) {
		List<Category> userInterests;
		if(user.getInterests().isEmpty() || user.getInterests() == null)
			userInterests = new ArrayList<Category>(Arrays.asList(Category.values()));
		else
			userInterests = new ArrayList<Category>(user.getInterests());
		return userInterests;
	}

	private static void runRules(XTTModel model, String[] tables)
			throws NotInTheDomainException {
		try{
		  	HeaRT.fixedOrderInference(model, tables);
		} catch(UnsupportedOperationException e){
		  	e.printStackTrace();
		}
	}

	private static void prepareBasicState(XTTModel model, User user, WeatherConditions weather)
			throws NotInTheDomainException {
		
		// CREATE STATE ELEMENTS
		// wind
		StateElement windInHmPH = new StateElement();
		windInHmPH.setAttributeName("windInHmPH");
		//temp
		StateElement tempInF = new StateElement();
		tempInF.setAttributeName("tempInF");
		// rain
		StateElement rainVal = new StateElement();
		rainVal.setAttributeName("rainVal");
		// age  
		StateElement ageNo = new StateElement();
		ageNo.setAttributeName("ageNumber");
		// place type
		StateElement placeType = new StateElement();
		placeType.setAttributeName("placeType");
		// user interests
		StateElement userInterest = new StateElement();
		userInterest.setAttributeName("userInterest");
		// user gender
		StateElement userGender = new StateElement();
		userGender.setAttributeName("userGender");
		
		// SET VALUES
		windInHmPH.setValue(new SimpleNumeric(weather.getWindStr()));
		tempInF.setValue(new SimpleNumeric(weather.getTemp()));
		rainVal.setValue(new SimpleNumeric(weather.getRain()));
		ageNo.setValue(new SimpleNumeric((double) user.getAge()));
		userInterest.setValue(new SimpleSymbolic(Category.FOOD.toString()));
		userGender.setValue(new SimpleSymbolic(user.getGender().toString()));
		placeType.setValue(new SetValue());

		// ADD BEGINNING STATE TO A MODEL
		State XTTstate = new State();
		LinkedList<StateElement> states = new LinkedList<StateElement>();
		states.add(windInHmPH);
		states.add(tempInF);
		states.add(rainVal);
		states.add(ageNo);
		states.add(userInterest);
		states.add(userGender);
		states.add(placeType);
	    XTTstate.setStateElements(states);
		
		model.setCurrentState(XTTstate);
	}
}
