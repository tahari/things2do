package pl.edu.agh.athings2do;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.edu.agh.athings2do.R;
import pl.edu.agh.athings2do.model.WeatherConditions;
import pl.edu.agh.athings2do.reasoning.PlaceTypesReasoner;

import com.aware.Aware;
import com.aware.Aware_Preferences;
import com.aware.plugin.user_profile.UserProfile_Provider.UserProfileInterests_Data;
import com.aware.plugin.user_profile.UserProfile_Provider.UserProfile_Data;
import com.aware.plugin.user_profile.model.Category;
import com.aware.plugin.user_profile.model.User;
import com.aware.plugin.user_profile.model.Gender;
import com.aware.plugin.weather.Plugin;
import com.aware.plugin.weather.Weather_Provider.WeatherCurrent;
import com.aware.providers.Locations_Provider.Locations_Data;
import com.aware.ui.Plugins_Manager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

/*
 * Based on tutorial:
 * MyMapActivity class forms part of Map application
 * in Mobiletuts+ tutorial series:
 * Using Google Maps and Google Places in Android Apps
 * 
 * This version of the class is for the final part of the series.
 * 
 * Sue Smith
 * March/ April 2013
 */

public class SearchPlacesActivity extends Activity {

	private int markerColor;

	private GoogleMap theMap;
	private Marker userMarker;	
	private Marker[] placeMarkers;		//places of interest
	private final int MAX_PLACES = 20;	// max; most returned from google
	private MarkerOptions[] places;		//marker options
	
	private LatLng lastLatLng;
	
	// !! To run correctly, generate your own API KEY to use Google Places API and paste it here!
	private static final String API_KEY = "AIzaSyDexQbwpUawXiQsCa9bMGiDbEPq39oFFiw";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_places);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		setSensorAndPluginSettings();
		//get drawable ID
		markerColor = R.drawable.user_point;

		//find out if we already have it
		if(theMap == null) {
			//get the map
			theMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.the_map)).getMap();
			//check in case map/ Google Play services not available
			if(theMap != null) {
				//ok - proceed
				theMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				//create marker array
				placeMarkers = new Marker[MAX_PLACES];

				//get location
				lastLatLng = getLocation();
				if(lastLatLng != null)
					// search for places
					searchPlaces();
			}

		}
	}
	
	/*
	 *  Getting location using AWARE Location Sensor
	 */
	private LatLng getLocation() {
		
		String[] tableColumns = { Locations_Data.LATITUDE, Locations_Data.LONGITUDE };
		Cursor location_data = getContentResolver().query(Locations_Data.CONTENT_URI, tableColumns, 
				null, null, Locations_Data.TIMESTAMP + " DESC LIMIT 1");
		
		if( location_data != null && location_data.moveToFirst() ) {
			
			double latitude = location_data.getDouble(location_data.getColumnIndex(Locations_Data.LATITUDE));
			double longitude = location_data.getDouble(location_data.getColumnIndex(Locations_Data.LONGITUDE));
	
			if( location_data != null && ! location_data.isClosed() ) 
				location_data.close();
			
			return new LatLng(latitude, longitude);
		}
		return null;
	}
	
	private void setSensorAndPluginSettings() {
		setLocationSettings();
		setWeatherSettings();
		setUserProfileSettings();
	}
	
	private void setLocationSettings() {
		Aware.setSetting(getContentResolver(), Aware_Preferences.STATUS_LOCATION_GPS, true);
		Aware.setSetting(getContentResolver(), Aware_Preferences.STATUS_LOCATION_NETWORK, true);
		Aware.setSetting(getContentResolver(), Aware_Preferences.FREQUENCY_GPS, 5);
		Aware.setSetting(getContentResolver(), Aware_Preferences.FREQUENCY_NETWORK, 15);
		Aware.setSetting(getContentResolver(), Aware_Preferences.MIN_GPS_ACCURACY, 150);
		Aware.setSetting(getContentResolver(), Aware_Preferences.MIN_NETWORK_ACCURACY, 250);
		Aware.setSetting(getContentResolver(), Aware_Preferences.EXPIRATION_TIME, 300);

		//Apply settings
		Intent applySettings = new Intent(Aware.ACTION_AWARE_REFRESH);
		sendBroadcast(applySettings);
	}
	

	private void setWeatherSettings() {		
		
		// Try to activate the plugin - if it is not available on the phone, it will automatically prompt the user to install it from AWARE's repository
		Intent openWeather = new Intent(Plugins_Manager.ACTION_AWARE_ACTIVATE_PLUGIN);
		openWeather.putExtra(Plugins_Manager.EXTRA_PACKAGE_NAME, "com.aware.plugin.openweather");
		sendBroadcast(openWeather);
	}
	
	private void setUserProfileSettings() {		
		
		Intent userprofile = new Intent(Plugins_Manager.ACTION_AWARE_ACTIVATE_PLUGIN);
		userprofile.putExtra(Plugins_Manager.EXTRA_PACKAGE_NAME, "com.aware.plugin.user_profile");
		sendBroadcast(userprofile);
	}
	
	/*
	 * Search for places and create markers
	 */
	private void searchPlaces(){
	
		//remove any existing marker
		if(userMarker != null) userMarker.remove();
		//create and set marker properties
		userMarker = theMap.addMarker(new MarkerOptions()
			.position(lastLatLng)
			.title("Tu jeste�!")
			.icon(BitmapDescriptorFactory.fromResource(markerColor))
			.snippet("Twoje ostatnio zarejestrowane po�o�enie."));
		//move to location
		theMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastLatLng, 15), 3000, null);
		
		String latVal=String.valueOf(lastLatLng.latitude);
		String lngVal=String.valueOf(lastLatLng.longitude);
		String placesSearchStr;
		try {
			// build places query string
			placesSearchStr = preparePlacesSearchStr(latVal, lngVal);
			// execute query
	        new GetPlaces().execute(placesSearchStr);
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }
	}
	
	private String preparePlacesSearchStr(String latVal, String lngVal)
			throws UnsupportedEncodingException {
		String placesSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="
		+ URLEncoder.encode(latVal, "UTF-8") +","
		+ URLEncoder.encode(lngVal, "UTF-8")
		+"&radius=" + URLEncoder.encode("500", "UTF-8")
		+"&sensor=" + URLEncoder.encode("true", "UTF-8")
		+"&key="    + URLEncoder.encode(API_KEY, "UTF-8");
		return placesSearchStr;
	}
	
	private class GetPlaces extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... URLs) {
			String types = getPlaceTypesFromContext();
			String placesStr = null;
			//fetch places
			//process search parameter string(s)			
			if(types != null) {
				for (String searchURL : URLs) {
					HttpClient httpClient = new DefaultHttpClient();
					try {
						//try to fetch the data
						// add types to a query
						searchURL += "&types=" + URLEncoder.encode(types, "UTF-8");
						placesStr = getStringFromURL(httpClient, searchURL);
					} catch(Exception e){ 
						e.printStackTrace(); 
					}
				}
			}
			
			return placesStr;
		}

		private String getPlaceTypesFromContext() {			
			WeatherConditions wc = getCurrentWeather();
			if(wc != null) {
				String types = getTypesByUserContext(wc);
				return typesDelPipe(types);
			}
			return null;
		}
		
		

		private String getTypesByUserContext(WeatherConditions wc) {
			
			User user = getUserProfile();
			return PlaceTypesReasoner.getTypes(getApplicationContext(), user, wc);
		}

		/*
		 * Getting weather using AWARE Weather Plugin
		 */
		protected WeatherConditions getCurrentWeather() {

			WeatherConditions wc = null;
			
			Cursor weather_data = getContentResolver().query(WeatherCurrent.CONTENT_URI, null, null, null,
					WeatherCurrent.FORECAST_TIMESTAMP + " DESC LIMIT 1");

			if( weather_data != null && weather_data.moveToFirst() ) {
				
				double weather_temperature 	= weather_data.getDouble(weather_data.getColumnIndex(WeatherCurrent.TEMPERATURE_VALUE_CURRENT));
				double weather_windspeed 	= weather_data.getDouble(weather_data.getColumnIndex(WeatherCurrent.WIND_SPEED));
				double weather_rain 		= weather_data.getDouble(weather_data.getColumnIndex(WeatherCurrent.RAIN));
				
				wc = new WeatherConditions(weather_temperature, weather_windspeed, weather_rain);
				
				//close the cursor to avoid memory leaks
				if( weather_data != null && ! weather_data.isClosed() ) 
					weather_data.close();
			}		
			return wc;
		}
		
		/*
		 * Getting user profile using my own AWARE UserProfile plugin
		 */
		private User getUserProfile() {
			User user = null;
			
			// get age & gender
			Cursor userprofile_data = getContentResolver().query(UserProfile_Data.CONTENT_URI, null, null, null,
					UserProfile_Data.TIMESTAMP + " DESC LIMIT 1");
			
			if( userprofile_data != null && userprofile_data.moveToFirst() ) {
				
				int user_age 		= userprofile_data.getInt(userprofile_data.getColumnIndex(UserProfile_Data.AGE));
				String user_gender 	= userprofile_data.getString(userprofile_data.getColumnIndex(UserProfile_Data.GENDER));
		
				user = new User(Gender.valueOf(user_gender), user_age, null);
				
				//close the cursor to avoid memory leaks
				if( userprofile_data != null && ! userprofile_data.isClosed() ) 
					userprofile_data.close();
			}	
		
			// get interests
			Cursor userprofile_interests_data = getContentResolver().query(UserProfileInterests_Data.CONTENT_URI, null, null, null,
					UserProfileInterests_Data.TIMESTAMP + " DESC LIMIT 1");
			
			if( userprofile_interests_data != null && userprofile_interests_data.moveToFirst() ) {
				
				List<Category> interests = new LinkedList<Category>();
				Category[] categories = Category.values();
				
				for(int i=0; i<UserProfileInterests_Data.INTERESTS.length; i++) {
					byte is_user_interest = (byte) userprofile_interests_data.getInt(
							userprofile_interests_data.getColumnIndex(UserProfileInterests_Data.INTERESTS[i]));
					if(is_user_interest == 1)
						interests.add(categories[i]);
					
				}
				user.setInterests(interests);
				
				//close the cursor to avoid memory leaks
				if( userprofile_interests_data != null && ! userprofile_interests_data.isClosed() ) 
					userprofile_interests_data.close();
			}	
			
			return user;
		}

		private String typesDelPipe(String types) {
			if(types == null || types.equals(""))
				return null;
			
			String typesNoBrackets = types.substring(1, types.length()-1);
			String typesWithPipes = typesNoBrackets.replace(" ,", "|");
			
			return typesWithPipes;
		}
		
		private String getStringFromURL(HttpClient httpClient,
				String searchURL) throws ClientProtocolException, IOException {
			StringBuilder URLBuilder = new StringBuilder();
			//HTTP Get receives URL string
			HttpGet httpGet = new HttpGet(searchURL);
			//execute GET with Client - return response
			HttpResponse httpResponse = httpClient.execute(httpGet);
			//check response status
			StatusLine searchStatus = httpResponse.getStatusLine();
			
			//only carry on if response is OK
			if (searchStatus.getStatusCode() == 200) {
				//get response
				HttpEntity httpEntity = httpResponse.getEntity();
				InputStream content = httpEntity.getContent();
				InputStreamReader input = new InputStreamReader(content);
				BufferedReader reader = new BufferedReader(input);
				//read a line at a time, append to string builder
				String lineIn;
				while ((lineIn = reader.readLine()) != null) {
					URLBuilder.append(lineIn);
				}
			}
			return URLBuilder.toString();
		}
		
		//process data retrieved from doInBackground
		protected void onPostExecute(String placesResult) {
			//parse place data returned from Google Places
			//remove existing markers
			if(placeMarkers != null){
				for(int pm=0; pm<placeMarkers.length; pm++){
					if(placeMarkers[pm] != null)
						placeMarkers[pm].remove();
				}
			}
			try {
				//parse JSON				
				//create JSONObject, pass string returned from doInBackground

				JSONObject resultObject = new JSONObject(placesResult);
				//get "results" array
				JSONArray placesArray = resultObject.getJSONArray("results");
				//marker options for each place returned
				places = new MarkerOptions[placesArray.length()];

				//loop through places
				for (int p=0; p<placesArray.length(); p++) {
					//parse each place
					//if any values are missing we won't show the marker
					boolean missingValue = false;
					LatLng placeLatLng = null;
					String placeName = "";
					String vicinity = "";
					int currIcon = 60;
					try {
						//attempt to retrieve place data values
						missingValue = false;
						//get place at this index
						JSONObject placeObject = placesArray.getJSONObject(p);
						//get location info
						JSONObject loc = placeObject.getJSONObject("geometry")
								.getJSONObject("location");
						placeLatLng = new LatLng(Double.valueOf(loc.getString("lat")), 
								Double.valueOf(loc.getString("lng")));	
						
						//get location type (first of all it's types)
						JSONArray types = placeObject.getJSONArray("types");	
						int categoryNo = PlaceTypesReasoner.typeToCategoryNo(getApplicationContext(), types.get(0).toString());
						// set icon color by type
						currIcon = categoryNo * 30;
						
						//vicinity
						vicinity = placeObject.getString("vicinity");
						//name
						placeName = placeObject.getString("name");
					}
					catch(JSONException jse){
						missingValue=true;
						jse.printStackTrace();
					}
					//if values missing we don't display
					if(missingValue)
						places[p]=null;
					else
						places[p]=new MarkerOptions()
					.position(placeLatLng)
					.title(placeName)
					.icon(BitmapDescriptorFactory.defaultMarker(currIcon))
					.snippet(vicinity);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			if(places != null && placeMarkers != null){

				for(int p=0; p<places.length && p<placeMarkers.length; p++){
					//will be null if a value was missing
					if(places[p] != null)
						placeMarkers[p] = theMap.addMarker(places[p]);
				}
			}
	
		}
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		// location
		Aware.setSetting(getContentResolver(), Aware_Preferences.STATUS_LOCATION_GPS, false);
		Aware.setSetting(getContentResolver(), Aware_Preferences.STATUS_LOCATION_NETWORK, false);

		// weather		
		Aware.setSetting(getContentResolver(), Plugin.ACTION_AWARE_WEATHER, false);

		//Apply settings
		Intent applySettings = new Intent(Aware.ACTION_AWARE_REFRESH);
		sendBroadcast(applySettings);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_places, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
	    switch (item.getItemId()) {
	    case R.id.action_refresh:
	    	//get location
	    	LatLng newLocation = getLocation();
	    	if(newLocation != null) {
	    		lastLatLng = newLocation;
	    		// search for places
				searchPlaces();
	    	}
	    	break;
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}

}
