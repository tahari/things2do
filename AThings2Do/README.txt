1. Potrzebne biblioteki:
a) google-play-services_lib
http://developer.android.com/google/play-services/setup.html
b) UserProfile
Aplikacja do pobrania z tego repozytorium.
c) Weather
Plugin dla AWARE dost�pny na http://svn.awareframework.com/ (source -> plugin -> Weather).

---------------------------------------

2. Uruchomienie aplikacji z kodu �r�d�owego.
Aplikacja korzysta z Google Maps i Google Places API, wi�c konieczne jest wygenerowanie w�asnego klucza i wklejenie go w:
a) AndroidManifest.xml (Google Maps)
...
<meta-data
    android:name="com.google.android.maps.v2.API_KEY"
    android:value="YOUR_API_KEY" />
...

b) SearchPlacesActivity.java (Google Places API)
...
private static final String API_KEY = "YOUR_API_KEY";
...