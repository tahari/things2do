package com.aware.plugin.user_profile;

import java.util.HashMap;

import com.aware.Aware;
import com.aware.plugin.user_profile.model.Category;
import com.aware.utils.DatabaseHelper;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Environment;
import android.provider.BaseColumns;
import android.util.Log;

public class UserProfile_Provider extends ContentProvider {

	/**
	* Authority of this content provider
	*/
	public static final String AUTHORITY = "com.aware.provider.plugin.user_profile";
	/**
	* ContentProvider database version. Increment every time you modify the database structure
	*/
	public static final int DATABASE_VERSION = 2;
	
	//ContentProvider query indexes
	private static final int USER_PROFILE = 1;
	private static final int USER_PROFILE_ID = 2;
	private static final int USER_PROFILE_INTERESTS = 3;
	private static final int USER_PROFILE_INTERESTS_ID = 4;
	
	public static final class UserProfile_Data implements BaseColumns {
		private UserProfile_Data(){};

		public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/plugin_user_profile");
		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.aware.plugin.user_profile";
		public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.aware.plugin.user_profile";
	
		public static final String _ID = "_id";
		public static final String TIMESTAMP = "timestamp";
		public static final String DEVICE_ID = "device_id";
		public static final String AGE = "age";
		public static final String GENDER = "gender";
	}
	
	public static final class UserProfileInterests_Data implements BaseColumns {
		private UserProfileInterests_Data(){};

		public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/plugin_user_profile_interests");
		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.aware.plugin.user_profile_interests";
		public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.aware.plugin.user_profile_interests";
	
		public static final String _ID = "_id";
		public static final String TIMESTAMP = "timestamp";
		public static final String DEVICE_ID = "device_id";
		public static final String[] INTERESTS = Category.valuesAsStringArray();
	}
	
	/**
	* Database stored in external folder: /AWARE/plugin_user_profile.db
	*/
	public static final String DATABASE_NAME = Environment.getExternalStorageDirectory() + "/AWARE/plugin_user_profile.db";
	
	/**
	* Database tables:<br/>
	* - plugin_user_profile
	*/
	public static final String[] DATABASE_TABLES = {"plugin_user_profile", "plugin_user_profile_interests"};
	
	/**
    * Database table fields
    */
    public static final String[] TABLES_FIELDS = {
        UserProfile_Data._ID + " integer primary key autoincrement," +
        UserProfile_Data.TIMESTAMP + " real default 0," +
        UserProfile_Data.DEVICE_ID + " text default ''," +
        UserProfile_Data.AGE + " integer default 18," +
        UserProfile_Data.GENDER + " text default 'M'," +
        "UNIQUE (" + UserProfile_Data.TIMESTAMP + "," + UserProfile_Data.DEVICE_ID +")",
        
        UserProfileInterests_Data._ID + " integer primary key autoincrement," +
        UserProfileInterests_Data.TIMESTAMP + " real default 0," +
        UserProfileInterests_Data.DEVICE_ID + " text default ''," +
        UserProfileInterests_Data.INTERESTS[0] + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[1] + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[2] + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[3] + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[4]  + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[5]  + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[6]  + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[7]  + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[8]  + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[9]  + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[10]  + " integer default 0," +
        UserProfileInterests_Data.INTERESTS[11]  + " integer default 0," +
        "UNIQUE (" + UserProfile_Data.TIMESTAMP + "," + UserProfile_Data.DEVICE_ID +")"
    };
    
    private static UriMatcher sUriMatcher = null;
    private static HashMap<String, String> userProfileMap = null;
	private static HashMap<String, String> userProfileInterestsMap = null;
    private static DatabaseHelper databaseHelper = null;
    private static SQLiteDatabase database = null;

    private boolean initializeDB() {
	    if (databaseHelper == null) {
	    	databaseHelper = new DatabaseHelper( getContext(), DATABASE_NAME, null, DATABASE_VERSION, DATABASE_TABLES, TABLES_FIELDS );
	    }
	    if( databaseHelper != null && ( database == null || ! database.isOpen() )) {
	    	database = databaseHelper.getWritableDatabase();
	    }
	    return( database != null && databaseHelper != null);
    }

    static {
	    sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	    sUriMatcher.addURI(AUTHORITY, DATABASE_TABLES[0], USER_PROFILE); //URI for all records
	    sUriMatcher.addURI(AUTHORITY, DATABASE_TABLES[0] + "/#", USER_PROFILE_ID); //URI for a single record
	    sUriMatcher.addURI(AUTHORITY, DATABASE_TABLES[1], USER_PROFILE_INTERESTS);
		sUriMatcher.addURI(AUTHORITY, DATABASE_TABLES[1] + "/#", USER_PROFILE_INTERESTS_ID);
	
	    userProfileMap = new HashMap<String, String>();
	    userProfileMap.put(UserProfile_Data._ID, UserProfile_Data._ID);
	    userProfileMap.put(UserProfile_Data.TIMESTAMP, UserProfile_Data.TIMESTAMP);
	    userProfileMap.put(UserProfile_Data.DEVICE_ID, UserProfile_Data.DEVICE_ID);
	    userProfileMap.put(UserProfile_Data.AGE, UserProfile_Data.AGE);
	    userProfileMap.put(UserProfile_Data.GENDER, UserProfile_Data.GENDER);
	    
	    userProfileInterestsMap = new HashMap<String, String>();
	    userProfileInterestsMap.put(UserProfileInterests_Data._ID, UserProfileInterests_Data._ID);
	    userProfileInterestsMap.put(UserProfileInterests_Data.TIMESTAMP, UserProfileInterests_Data.TIMESTAMP);
	    userProfileInterestsMap.put(UserProfileInterests_Data.DEVICE_ID, UserProfileInterests_Data.DEVICE_ID);
	    for(int i=0; i<UserProfileInterests_Data.INTERESTS.length; i++)
	    	userProfileInterestsMap.put(UserProfileInterests_Data.INTERESTS[i], UserProfileInterests_Data.INTERESTS[i]);
    }
    
	@Override
	public boolean onCreate() {
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		if( ! initializeDB() ) {
			Log.w(AUTHORITY,"Database unavailable...");
			return null;
		}

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		switch (sUriMatcher.match(uri)) {
		case USER_PROFILE:
			qb.setTables(DATABASE_TABLES[0]);
			qb.setProjectionMap(userProfileMap);
			break;
		case USER_PROFILE_INTERESTS:
			qb.setTables(DATABASE_TABLES[1]);
			qb.setProjectionMap(userProfileInterestsMap);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		try {
			Cursor c = qb.query(database, projection, selection, selectionArgs,
			null, null, sortOrder);
			c.setNotificationUri(getContext().getContentResolver(), uri);
			return c;
		} catch (IllegalStateException e) {
			if (Aware.DEBUG)
				Log.e(Aware.TAG, e.getMessage());
			return null;
		}
	}

	@Override
	public String getType(Uri uri) {
		switch (sUriMatcher.match(uri)) {
		case USER_PROFILE:
			return UserProfile_Data.CONTENT_TYPE;
		case USER_PROFILE_ID:
			return UserProfile_Data.CONTENT_ITEM_TYPE;
		case USER_PROFILE_INTERESTS:
			return UserProfileInterests_Data.CONTENT_TYPE;
		case USER_PROFILE_INTERESTS_ID:
			return UserProfileInterests_Data.CONTENT_ITEM_TYPE;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues new_values) {
		if( ! initializeDB() ) {
			Log.w(AUTHORITY,"Database unavailable...");
			return null;
		}

		ContentValues values = (new_values != null) ? new ContentValues(new_values) : new ContentValues();

		switch (sUriMatcher.match(uri)) {
		case USER_PROFILE:
			long up_id = database.insert(DATABASE_TABLES[0],
					UserProfile_Data.DEVICE_ID, values);
			if (up_id > 0) {
				Uri upDataUri = ContentUris.withAppendedId(
						UserProfile_Data.CONTENT_URI, up_id);
				getContext().getContentResolver().notifyChange(upDataUri, null);
				return upDataUri;
			}
			throw new SQLException("Failed to insert row into " + uri);
		case USER_PROFILE_INTERESTS:
			long upi_id = database.insert(DATABASE_TABLES[1],
					UserProfileInterests_Data.DEVICE_ID, values);

			if (upi_id > 0) {
				Uri upiDataUri = ContentUris.withAppendedId(
						UserProfileInterests_Data.CONTENT_URI, upi_id);
				getContext().getContentResolver().notifyChange(upiDataUri,
						null);
				return upiDataUri;
			}
			throw new SQLException("Failed to insert row into " + uri);
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		if( ! initializeDB() ) {
			Log.w(AUTHORITY,"Database unavailable...");
			return 0;
		}

		int count = 0;
		switch (sUriMatcher.match(uri)) {
		case USER_PROFILE:
			count = database.delete(DATABASE_TABLES[0], selection,
					selectionArgs);
			break;
		case USER_PROFILE_INTERESTS:
			count = database.delete(DATABASE_TABLES[1], selection,
					selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		if( ! initializeDB() ) {
			Log.w(AUTHORITY,"Database unavailable...");
			return 0;
		}

		int count = 0;
		switch (sUriMatcher.match(uri)) {
		case USER_PROFILE:
			count = database.update(DATABASE_TABLES[0], values, selection,
					selectionArgs);
			break;
		case USER_PROFILE_INTERESTS:
			count = database.update(DATABASE_TABLES[1], values, selection,
					selectionArgs);
			break;
		default:
			database.close();
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

}
