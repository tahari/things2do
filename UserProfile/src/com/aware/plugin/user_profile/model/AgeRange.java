package com.aware.plugin.user_profile.model;

public class AgeRange {

	/**
	 * @param ageToCheck wiek osoby
	 * @return czy wiek podanej osoby znajduje sie w wyznaczonym zakresie wiekowym
	 */
	public enum AgeRanges {
	
		CHILD, TEEN, ADULT, ELDER;
		
	}

	public static AgeRanges parseAge(int age) {
		if(age < 11)
			return AgeRanges.CHILD;
		else if(age < 18)
			return AgeRanges.TEEN;
		else if(age < 60)
			return AgeRanges.ADULT;

		return AgeRanges.ELDER;
	}

}
