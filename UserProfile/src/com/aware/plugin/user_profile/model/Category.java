package com.aware.plugin.user_profile.model;

public enum Category {
		FOOD,
		TRANSPORT,
		ART_CULTURE,
		SPORT_ENTERTAINMENT_RECREATION,
		TOURISM,
		ACCOMMODATION,
		OFFICES_INSTITUTIONS,
		HEALTH_BEAUTY,
		RELIGION,
		EDUCATION,
		FINANCE,
		SHOP_SERVICE;
		
		public static String[] valuesAsStringArray() {
			   Category[] categories = Category.values();
			   String[] categoriesStr = new String[categories.length];
			   
			   for(int i=0; i<categories.length; i++)
				   categoriesStr[i] = categories[i].toString();
			   
			   return categoriesStr;
		}
}
