package com.aware.plugin.user_profile.model;

import java.util.LinkedList;
import java.util.List;

public class User {
	
	public static final int DEFAULT_AGE = 18;
	public static final Gender DEFAULT_GENDER = Gender.M;

	private Gender gender;
	private int age;
	private List<Category> interests;
	
	public User() {
		this.gender = DEFAULT_GENDER;
		this.age = DEFAULT_AGE;
		this.interests = new LinkedList<>();
	}
	
	public User(Gender gender, int age, List<Category> interests) {
;
		this.gender = gender;
		this.age = age;
		if(interests == null)
			this.interests = new LinkedList<>();
		else
			this.interests = new LinkedList<>(interests);
	}
	
	public User(User user) {
		this.gender = user.getGender();
		this.age = user.getAge();
		if(user.getInterests() == null)
			this.interests = new LinkedList<>();
		else
			this.interests = new LinkedList<>(user.getInterests());
	}

	public Gender getGender() {
		return gender;
	}

	public int getAge() {
		return age;
	}

	public List<Category> getInterests() {
		return interests;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setInterests(List<Category> interests) {
		this.interests = new LinkedList<Category>(interests);
	}
	
	public void setInterests(byte[] interests) {
		this.interests.clear();
		int i = 0;
		for(byte isInterest: interests) {
			if(isInterest == 1) 
				this.interests.add(Category.values()[i]);
			i++;
		}
	}

	public void setGender(String gender) {
		if(gender != null)
			this.gender = Gender.valueOf(gender);
		else
			this.gender = DEFAULT_GENDER;
	}

	@Override
	public String toString() {
		return "User [gender=" + gender + ", age=" + age
				+ ", interests=]";
	}
}
