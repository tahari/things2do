package com.aware.plugin.user_profile;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;

import com.aware.Aware;
import com.aware.Aware_Preferences;
import com.aware.plugin.user_profile.UserProfile_Provider.UserProfileInterests_Data;
import com.aware.plugin.user_profile.UserProfile_Provider.UserProfile_Data;
import com.aware.utils.Aware_Plugin;

public class Plugin extends Aware_Plugin {

	/**
	* Broadcasted event: the user changed her/his profile
	*/
	public static final String ACTION_AWARE_PLUGIN_USER_PROFILE = "ACTION_AWARE_PLUGIN_USER_PROFILE";

	//private variables that hold the latest values to be shared whenever ACTION_AWARE_CURRENT_CONTEXT is broadcasted
	private static int upAge;
	private static String upGender;
	private static byte[] upInterests;
	
    /**
     * BroadcastReceiver that will receiver screen ON events from AWARE
     */
    private UserProfileListener userProfileListener = new UserProfileListener();
    public class UserProfileListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
        	upAge = intent.getIntExtra(Settings.EXTRA_USERPROFILE_AGE, 18);
        	upGender = intent.getStringExtra(Settings.EXTRA_USERPROFILE_GENDER);
        	upInterests = intent.getByteArrayExtra(Settings.EXTRA_USERPROFILE_INTERESTS);         
            //Share context
            CONTEXT_PRODUCER.onContext();
        }
    }
	
	@Override
	public void onCreate() {
		super.onCreate();
		//Code here when add-on is turned on.
		
		//create a context filter
        IntentFilter filter = new IntentFilter();
		filter.addAction(Settings.ACTION_AWARE_SETTINGS_PLUGIN_USER_PROFILE);
        
        //Ask Android to register our context receiver
		registerReceiver(userProfileListener, filter);
        
		//Shares this plugin's context to AWARE and applications
		CONTEXT_PRODUCER = new ContextProducer() {
			@Override
			public void onContext() {
				Intent context = new Intent(ACTION_AWARE_PLUGIN_USER_PROFILE);
				sendBroadcast(context);
				
				long currTime = System.currentTimeMillis();
				
                ContentValues context_data = new ContentValues();
                context_data.put(UserProfile_Data.TIMESTAMP, currTime);
                context_data.put(UserProfile_Data.DEVICE_ID, Aware.getSetting(getContentResolver(), Aware_Preferences.DEVICE_ID));
                context_data.put(UserProfile_Data.AGE, upAge);
                context_data.put(UserProfile_Data.GENDER, upGender);
                
                ContentValues context_data_interests = new ContentValues();
                context_data_interests.put(UserProfileInterests_Data.TIMESTAMP, currTime);
                context_data_interests.put(UserProfileInterests_Data.DEVICE_ID, Aware.getSetting(getContentResolver(), 
                		Aware_Preferences.DEVICE_ID));
                for(int i=0; i<UserProfileInterests_Data.INTERESTS.length; i++)
                	context_data_interests.put(UserProfileInterests_Data.INTERESTS[i], upInterests[i]);
                
                //insert data to table UserProfile_Data
                getContentResolver().insert(UserProfile_Data.CONTENT_URI, context_data);
                //insert data to table UserProfileInterests_Data
                getContentResolver().insert(UserProfileInterests_Data.CONTENT_URI, context_data_interests);
			}
		};
		
		 //Our provider tables
        DATABASE_TABLES = UserProfile_Provider.DATABASE_TABLES;
        //Our table fields
        TABLES_FIELDS = UserProfile_Provider.TABLES_FIELDS;
        //Our provider URI
        CONTEXT_URIS = new Uri[]{ UserProfile_Data.CONTENT_URI, UserProfileInterests_Data.CONTENT_URI };
	
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		//Code here when add-on is turned off.
		//unregister our screen context receiver from Android
		unregisterReceiver(userProfileListener);
	}
}
