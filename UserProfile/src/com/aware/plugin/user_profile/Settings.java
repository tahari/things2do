package com.aware.plugin.user_profile;

import com.aware.plugin.user_profile.UserProfile_Provider.UserProfileInterests_Data;
import com.aware.plugin.user_profile.UserProfile_Provider.UserProfile_Data;
import com.aware.plugin.user_profile.model.Category;
import com.aware.plugin.user_profile.model.Gender;
import com.aware.plugin.user_profile.model.User;
import com.aware.ui.Plugins_Manager;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

public class Settings extends Activity {

	private EditText etUserAge;
	private Spinner sUserGender;
	private CheckBox[] cbUserInterests;
	
	public static final String ACTION_AWARE_SETTINGS_PLUGIN_USER_PROFILE = "ACTION_AWARE_SETTINGS_PLUGIN_USER_PROFILE";
	public static final String EXTRA_USERPROFILE_AGE = "userprofile_age";
	public static final String EXTRA_USERPROFILE_GENDER = "userprofile_gender";
	public static final String EXTRA_USERPROFILE_INTERESTS = "userprofile_interests";
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.settings);
        
        initView();
        
        //Ask AWARE to activate our plugin if not active
        Intent activateUserProfile = new Intent(Plugins_Manager.ACTION_AWARE_ACTIVATE_PLUGIN);
        activateUserProfile.putExtra(Plugins_Manager.EXTRA_PACKAGE_NAME, getPackageName());
        sendBroadcast(activateUserProfile);
	}
	
	private void initView() {
		//age
		etUserAge	 	= (EditText) findViewById(R.id.etUserAge);
		//gender
		sUserGender		= (Spinner) findViewById(R.id.sUserGender);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.gender, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sUserGender.setAdapter(adapter);
		// interests
		cbUserInterests = new CheckBox[Category.values().length];
		cbUserInterests[0] = (CheckBox) findViewById(R.id.cbCategory1);
		cbUserInterests[1] = (CheckBox) findViewById(R.id.cbCategory2);
		cbUserInterests[2] = (CheckBox) findViewById(R.id.cbCategory3);
		cbUserInterests[3] = (CheckBox) findViewById(R.id.cbCategory4);
		cbUserInterests[4] = (CheckBox) findViewById(R.id.cbCategory5);
		cbUserInterests[5] = (CheckBox) findViewById(R.id.cbCategory6);
		cbUserInterests[6] = (CheckBox) findViewById(R.id.cbCategory7);
		cbUserInterests[7] = (CheckBox) findViewById(R.id.cbCategory8);
		cbUserInterests[8] = (CheckBox) findViewById(R.id.cbCategory9);
		cbUserInterests[9] = (CheckBox) findViewById(R.id.cbCategory10);
		cbUserInterests[10] = (CheckBox) findViewById(R.id.cbCategory11);
		cbUserInterests[11] = (CheckBox) findViewById(R.id.cbCategory12);

		String[] categories = getResources().getStringArray(R.array.categories);
		for(int i=0; i<cbUserInterests.length; i++)
			cbUserInterests[i].setText(categories[i]);
	}

	@Override
	protected void onPause() {
		super.onPause();
			User newUser = getUserFromFieldsNoInterests();
			byte[] userInterests = getUserFromFieldsInterests();
			updateUserProfile(newUser, userInterests);
	}

	private User getUserFromFieldsNoInterests() {
		int age = Integer.valueOf(
				etUserAge.getText().toString());
		Gender gender = Gender.values()[(int) sUserGender.getSelectedItemId()];
		
		return new User(gender, age, null);
	}
	
	private byte[] getUserFromFieldsInterests() {
		byte[] interests = new byte[Category.values().length];

		Category[] categories = Category.values();
		for(int i=0; i<categories.length; i++)
			if(cbUserInterests[i].isChecked())
				interests[i] = 1;
			else
				interests[i] = 0;

		return interests;
	}

	private void updateUserProfile(User userNoInterests, byte[] userInterests) {
		Intent userContext = new Intent(ACTION_AWARE_SETTINGS_PLUGIN_USER_PROFILE);
		userContext.putExtra(EXTRA_USERPROFILE_AGE, 		userNoInterests.getAge());
		userContext.putExtra(EXTRA_USERPROFILE_GENDER, 		userNoInterests.getGender().toString());
		userContext.putExtra(EXTRA_USERPROFILE_INTERESTS, 	userInterests);
		sendBroadcast(userContext);
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadUserProfile();
	}

	private void loadUserProfile() {
		User user = new User();
		// age, gender
		Cursor user_profile = getContentResolver().query(UserProfile_Data.CONTENT_URI, null, null, null, 
				UserProfile_Data.TIMESTAMP + " DESC");
		if( user_profile != null && user_profile.moveToFirst() ) {
			int age = user_profile.getInt(user_profile.getColumnIndex(UserProfile_Data.AGE));
			String gender = user_profile.getString(user_profile.getColumnIndex(UserProfile_Data.GENDER));							
			user_profile.close();
			user.setAge(age);
			user.setGender(gender);
		}
		
		// interests		
		Cursor user_profile_interests = getContentResolver().query(UserProfileInterests_Data.CONTENT_URI, null, null, null, 
				UserProfileInterests_Data.TIMESTAMP + " DESC");
		if( user_profile_interests != null && user_profile_interests.moveToFirst() ) {
			byte[] interests = new byte[Category.values().length];
			for(int i=0; i<interests.length; i++)
				interests[i] = (byte) user_profile_interests.getInt(
						user_profile_interests.getColumnIndex(UserProfileInterests_Data.INTERESTS[i]));
			user.setInterests(interests);
		}

		loadUserProfileView(user);
	}

	private void loadUserProfileView(User user) {
		
		etUserAge.setText(String.valueOf(user.getAge()));
		if(user.getGender() == null)
			sUserGender.setSelection(User.DEFAULT_GENDER.ordinal());
		else
			sUserGender.setSelection(user.getGender().ordinal());
		for(Category interest: user.getInterests())
			cbUserInterests[interest.ordinal()].setChecked(true);
	}
}
