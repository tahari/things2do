package pl.edu.agh.gthings2do.model.user;

import java.util.LinkedList;
import java.util.List;

public class User {
	
	private List<String> interests;
	
	public User() {
		this.interests = new LinkedList<String>();
	}

	public User(List<String> interests) {
		if(interests == null)
			this.interests = new LinkedList<String>();
		else
			this.interests = new LinkedList<String>(interests);
	}

	public List<String> getInterests() {
		return interests;
	}

	public void setInterests(List<String> interests) {
		this.interests = interests;
	}

	@Override
	public String toString() {
		return "User [interests=]";
	}
}
