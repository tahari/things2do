package pl.edu.agh.gthings2do.model.user;

public class AgeRange {

	public enum AgeRanges {
	
		CHILD, TEEN, ADULT, ELDER;
		
	}

	public static AgeRanges parseAge(int age) {
		if(age < 11)
			return AgeRanges.CHILD;
		else if(age < 18)
			return AgeRanges.TEEN;
		else if(age < 60)
			return AgeRanges.ADULT;

		return AgeRanges.ELDER;
	}

}
