package pl.edu.agh.gthings2do.model.place;

public enum Category {
	Automotive,
	Entertainment,
	Finance,
	Food,
	Kids,
	News,
	Parenting,
	Retail,
	Sports,
	Technology,
	Travel;
	
	public static Category parseCategory(String category) {
		String[] mainCategory = category.split("\\.");
		return Category.valueOf(mainCategory[0]);
	}
	
	public static String[] toStringArray() {
		String[] allCategories = new String[Category.values().length];
		int i = 0;
		for(Category category: Category.values()) {
			allCategories[i] = category.toString();
			i++;
		}
		
		return allCategories;
	}
}
