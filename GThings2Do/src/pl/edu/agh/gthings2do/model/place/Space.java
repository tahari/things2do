package pl.edu.agh.gthings2do.model.place;

import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.gthings2do.model.WeatherConditions;
import pl.edu.agh.gthings2do.model.WeatherConditions.Temperature;
import pl.edu.agh.gthings2do.model.WeatherConditions.WindStrenght;

public class Space {

	private KindOfSpace kindOfSpace;
	private WeatherConditions weatherNotCond;	// weather conditions not recommended for that kind of space
	
	public enum KindOfSpace {
		
		CLOSED, OPEN, COVERED, UNKNOWN;
		
	}
	
	public Space(KindOfSpace kindOfSpace) {
		this.kindOfSpace = kindOfSpace;

		switch(kindOfSpace) {
			case CLOSED:
			case UNKNOWN:
				this.weatherNotCond = new WeatherConditions(null, null, null); 
				break;
			case OPEN:
				this.weatherNotCond = new WeatherConditions(Temperature.COLD, WindStrenght.STRONG, true);
			case COVERED:
				this.weatherNotCond = new WeatherConditions(Temperature.COLD, WindStrenght.STRONG, null);
				break;
		}
	}
	
	public static List<KindOfSpace> getKindsOfSpaceOkForWeather(WeatherConditions weather) {
		List<KindOfSpace> kindsOfSpace = new LinkedList<Space.KindOfSpace>();
		
		kindsOfSpace.add(KindOfSpace.CLOSED);
		// OPENED
		WeatherConditions notCond = new WeatherConditions(Temperature.COLD, WindStrenght.STRONG, true);
		if(!weather.satisfiedByAnyCond(notCond))
			kindsOfSpace.add(KindOfSpace.OPEN);
		
		// COVERED
		notCond = new WeatherConditions(Temperature.COLD, WindStrenght.STRONG, null);
		if(!weather.satisfiedByAnyCond(notCond))
			kindsOfSpace.add(KindOfSpace.COVERED);
		
		return kindsOfSpace;
	}

	public KindOfSpace getKindOfSpace() {
		return kindOfSpace;
	}

	public WeatherConditions getWeatherNotCond() {
		return weatherNotCond;
	}
	
}
