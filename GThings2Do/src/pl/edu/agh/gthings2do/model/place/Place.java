package pl.edu.agh.gthings2do.model.place;

import java.util.Arrays;

import com.google.android.gms.maps.model.LatLng;

import pl.edu.agh.gthings2do.model.place.Space.KindOfSpace;
import pl.edu.agh.gthings2do.model.user.AgeRange.AgeRanges;
import pl.edu.agh.gthings2do.model.user.Gender;

public class Place {

	private long id;
	private String name;
	private String[] categories;
	private Gender targetGender;
	private KindOfSpace kindOfSpace;
	private AgeRanges[] ageRanges;
	private LatLng location;

	public Place(long id, String name, String[] categories, String targetGender, String kindOfSpace,
			String[] ageRanges, LatLng location) {
		// id
		this.id = id;
		// name
		this.name = name;
		// categories
		if(categories == null)
			this.categories = null;
		else {
			this.categories = new String[categories.length];
			for(int i=0; i<categories.length; i++)
				this.categories[i] = categories[i];
		}		
		// gender
		if(targetGender == null || targetGender.equals(""))
			this.targetGender = null;
		else
			this.targetGender = Gender.valueOf(targetGender);
		// kind of space
		if(kindOfSpace == null || kindOfSpace.equals(""))
			this.kindOfSpace = Space.KindOfSpace.CLOSED;
		else
			this.kindOfSpace = KindOfSpace.valueOf(kindOfSpace);
		// age ranges
		if(ageRanges == null || ageRanges[0].equals(""))
			this.ageRanges = null;
		else {			
			this.ageRanges = new AgeRanges[ageRanges.length];
			for(int i=0; i<ageRanges.length; i++)
				this.ageRanges[i] = AgeRanges.valueOf(ageRanges[i]);
		}
		// location
		this.location = location;
	}

	public long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String[] getCategories() {
		return categories;
	}

	public Gender getTargetGender() {
		return targetGender;
	}

	public KindOfSpace getKindOfSpace() {
		return kindOfSpace;
	}

	public AgeRanges[] getAgeRanges() {
		return ageRanges;
	}	
	
	public LatLng getLocation() {
		return location;
	}

	@Override
	public String toString() {
		return "Place [id=" + id + ", name=" + name + ", category="
				+ Arrays.toString(categories) + ", targetGender=" + targetGender
				+ ", kindOfSpace=" + kindOfSpace + ", ageRanges="
				+ Arrays.toString(ageRanges) + ", location=" + location + "]";
	}

	public Category getFirstCategory() {
		return Category.parseCategory(categories[0]);
	}

}
