package pl.edu.agh.gthings2do.model;


public class WeatherConditions {
	
	public enum Temperature {
		COLD, WARM
	}
	
	public enum WindStrenght {
		NONE, STRONG
	}
	
	// null - no matter/no data
	private Temperature temp;
	private WindStrenght windStr;
	private Boolean precip;
	
	public WeatherConditions(Temperature temp, WindStrenght windStr,
			Boolean precip) {
		initWeatherConditions(temp, windStr, precip);
	}

	public WeatherConditions(double temp, double windStr, Boolean precip) {
		Temperature tempAsEnum = parseTemperatureInK(temp);
		WindStrenght windStrAsEnum = parseWindInKmPH(windStr);
		
		initWeatherConditions(tempAsEnum, windStrAsEnum, precip);
	}

	private void initWeatherConditions(Temperature temp,
			WindStrenght windStr, Boolean precip) {
		this.temp = temp;
		this.windStr = windStr;
		this.precip = precip;
	}

	public Temperature parseTemperatureInK(double temperature) {
		if(temperature > 282.15)	// ~9stC
			return Temperature.WARM;

		return Temperature.COLD;
	}
	
	public WindStrenght parseWindInKmPH(double windStrenght) {
		if(windStrenght < 12)
			return WindStrenght.NONE;

		return WindStrenght.STRONG;
	}
	// http://www.mojapogoda.com/leksykon-meteorologiczny/wiatr.html	
	
	public boolean satisfiedByAnyCond(WeatherConditions wCondToCheck) {
		
		if((wCondToCheck.temp != null
				&& wCondToCheck.temp == this.temp) || this.temp == null)
				return true;
		
		if((wCondToCheck.windStr != null 
				&& wCondToCheck.windStr == this.windStr) || this.windStr == null)
				return true;
		
		if((wCondToCheck.precip != null 
				&& wCondToCheck.precip == this.precip) || this.precip == null)
				return true;
		
		return false;
	}

	@Override
	public String toString() {
		return "WeatherConditions [temp=" + temp + ", windStr=" + windStr
				+ ", rain=" + precip + "]";
	}
	
}
