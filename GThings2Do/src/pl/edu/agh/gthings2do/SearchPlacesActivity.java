package pl.edu.agh.gthings2do;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.edu.agh.gthings2do.model.WeatherConditions;
import pl.edu.agh.gthings2do.model.place.Category;
import pl.edu.agh.gthings2do.model.place.Place;
import pl.edu.agh.gthings2do.model.place.Space;
import pl.edu.agh.gthings2do.model.place.Space.KindOfSpace;
import pl.edu.agh.gthings2do.model.user.User;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.qualcommlabs.usercontext.Callback;
import com.qualcommlabs.usercontext.ContextCoreConnector;
import com.qualcommlabs.usercontext.ContextCoreConnectorFactory;
import com.qualcommlabs.usercontext.ContextInterestsConnector;
import com.qualcommlabs.usercontext.ContextInterestsConnectorFactory;
import com.qualcommlabs.usercontext.ContextPlaceConnector;
import com.qualcommlabs.usercontext.ContextPlaceConnectorFactory;
import com.qualcommlabs.usercontext.PlaceEventListener;
import com.qualcommlabs.usercontext.protocol.GeoFence;
import com.qualcommlabs.usercontext.protocol.PlaceAttributes;
import com.qualcommlabs.usercontext.protocol.PlaceEvent;
import com.qualcommlabs.usercontext.protocol.profile.AttributeCategory;
import com.qualcommlabs.usercontext.protocol.profile.Profile;
import com.qualcommlabs.usercontext.protocol.profile.ProfileAttribute;

import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

/*
 * Based on tutorial:
 * MyMapActivity class forms part of Map application
 * in Mobiletuts+ tutorial series:
 * Using Google Maps and Google Places in Android Apps
 * 
 * This version of the class is for the final part of the series.
 * 
 * Sue Smith
 * March/ April 2013
 */
public class SearchPlacesActivity extends FragmentActivity implements LocationListener {

	private GoogleMap theMap;
	private List<Marker> placeMarkers;		//places of interest
	private MarkerOptions markerOpts;		//marker options
	private Map<Long, String> markerIdsMap;
	
	private static final String CATEGORY_ATTR 		= "category";
	//private static final String AGE_ATTR 			= "target_age";
	//private static final String GENDER_ATTR 		= "target_gender";
	private static final String KINDOFSPACE_ATTR 	= "kind_of_space";
	
	public static final double INTEREST_MIN_LIKELIHOOD = 0.5;
	
	private User user = null;
	private List<Place> detectedPlaces;

	private ContextCoreConnector contextCoreConnector;
	private ContextPlaceConnector contextPlaceConnector;
	private ContextInterestsConnector contextInterestsConnector;
    private PlaceEventListener placeEventListener = new PlaceEventListener() {

            @Override
            public void placeEvent(PlaceEvent event) {

                String placeInfo = "id: " + event.getPlace().getId() + " name: " + event.getPlace().getPlaceName()
                		 + " eventType: " + event.getEventType() + " placeType: " + event.getPlaceType()
                		 + " geofence: " + event.getPlace().getGeoFence();
                Log.d("Place info", placeInfo);
                
                PlaceAttributes placeAttrs = event.getPlace().getPlaceAttributes();
                Log.d("Place attr", event.getPlace().getPlaceAttributes().toString());
               
                // check if a user enters location
                // a user enters location => add marker
                if(event.getEventType().equals(PlaceEvent.PLACE_EVENT_TYPE_AT)) {
                	detectedPlaces.add(createPlaceFromAttributes(event,
							placeAttrs));
                	new FilterPlace().execute(detectedPlaces.size() - 1);
                } else {
                	// a user leaves location => remove marker
                	removeMarker(event.getPlace().getId());
                }
            }
        };
        
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_places);

		//find out if we already have it
		if(theMap == null){
			//get the map
			SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.the_map);
            theMap = fm.getMap();
			//check in case map/ Google Play services not available
            theMap.setMyLocationEnabled(true);
			if(theMap != null){
				setupCoreConnectors();
				getUserProfile();
				//ok - proceed
				theMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				detectedPlaces = new LinkedList<Place>();
				//create marker array
				placeMarkers = new LinkedList<Marker>();
				markerIdsMap = new HashMap<Long, String>();
				cleanMarkers();
			}

		}
	}
	
	private void setupCoreConnectors() {
		contextCoreConnector = ContextCoreConnectorFactory.get(this);
	    contextPlaceConnector = ContextPlaceConnectorFactory.get(this);
        contextInterestsConnector = ContextInterestsConnectorFactory.get(this);

	    checkContextConnectorStatus();
	}

	private void checkContextConnectorStatus() {
        if (contextCoreConnector.isPermissionEnabled()) {
            // Gimbal is already enabled
        	startListeningForGeofences();
        }
        else {
             contextCoreConnector.enable(this, new Callback<Void>() {

                @Override
                public void success(Void arg0) {
                    // Gimbal is ready
                	startListeningForGeofences();
                }

                @Override
                public void failure(int arg0, String arg1) {
                    Log.e("failed to enable", arg1);
                }
            });
        }
	}

	private void startListeningForGeofences() {
		contextPlaceConnector.addPlaceEventListener(placeEventListener);
	}

	private void getUserProfile() {
		contextInterestsConnector.requestProfile(new Callback<Profile>() { 
        	@Override 
        	public void success(Profile profile) { 
        		// do something with profile 
        		parseUserProfile(profile);
        	} 
        	@Override 
        	public void failure(int statusCode, String errorMessage) { 
        		// failed with errorCode 
        	} 
        }); 
	}
	
	protected void parseUserProfile(Profile profile) {
		user = new User();
		
		if(profile == null)
			return;

		user = new User();
        Map<String, ProfileAttribute> userProfileAttrs = profile.getAttributes();

 		// get interests
        ProfileAttribute userInterests = userProfileAttrs.get("Interests");
        List<AttributeCategory> userInterestsList = userInterests.getAttributeCategories();		
		// unknown interests => all interests
		if(userInterestsList == null || userInterestsList.isEmpty())
			return;
		
		List<String> interestsAsStringList = new LinkedList<String>();
		for(AttributeCategory interest: userInterestsList) {
				// min likelihood is satisfied
         		if(interest.getLikelihood() > INTEREST_MIN_LIKELIHOOD) {
         			interestsAsStringList.add(interest.getKey());
         		}			
     	}
		user.setInterests(interestsAsStringList);
	}

	private Place createPlaceFromAttributes(PlaceEvent event,
			PlaceAttributes placeAttrs) {
		String[] categories = extractCategoriesFromAttribute(placeAttrs.get(CATEGORY_ATTR));
		//String[] targetAge = extractAgeFromAttribute(placeAttrs.get(AGE_ATTR));
		//String targetGender = placeAttrs.get(GENDER_ATTR);
		String kindOfSpace = extractKindOfSpaceFromAttribute(placeAttrs.get(KINDOFSPACE_ATTR));
		LatLng location = extractLocationFromGeofence(event.getPlace().getGeoFence());
		//Place detectedPlace = new Place(event.getPlace().getId(), event.getPlace().getPlaceName(),
		//		categories, targetGender, kindOfSpace, targetAge, location);
		Place detectedPlace = new Place(event.getPlace().getId(), event.getPlace().getPlaceName(),
				categories, null, kindOfSpace, null, location);
		
		return detectedPlace;
	}
	
	private String[] extractCategoriesFromAttribute(String categories) {
		if(categories != null)
			return categories.split(",");
		return Category.toStringArray();
				
	}
	
/*	private String[] extractAgeFromAttribute(String ages) {
		return ages.split(",");
	}*/
	
	private String extractKindOfSpaceFromAttribute(String kindOfSpace) {
		if(kindOfSpace != null)
			return kindOfSpace;
		return KindOfSpace.CLOSED.toString();				
	}
	
	private LatLng extractLocationFromGeofence(GeoFence geoFence) {
		double[] location = new double[2];
		
		Pattern pattern = Pattern.compile("([0-9]+)\\.([0-9]*)");
		Matcher matcher = pattern.matcher(geoFence.toString());
		int i = 0;
		while(matcher.find()) {
			location[i] = Double.parseDouble(matcher.group());
			i++;
		}
		return new LatLng(location[0], location[1]);
	}
	
	private void cleanMarkers() {
		//remove existing markers
		if(placeMarkers != null)
			placeMarkers.clear();
	}

// MARKERS
	private void addMarker(Place place) {		
		markerOpts = new MarkerOptions();
		
		LatLng placeLatLng = place.getLocation();
		int currIcon = place.getFirstCategory().ordinal() * 30;

		markerOpts = new MarkerOptions()
			.position(placeLatLng)
			.title(place.getName())
			.icon(BitmapDescriptorFactory.defaultMarker(currIcon));

		if(markerOpts != null && placeMarkers != null) {
			// add marker
			Marker newMarker = theMap.addMarker(markerOpts);
			placeMarkers.add(newMarker);
			// add mapping between placeId & markerId for same place
			markerIdsMap.put(place.getId(), newMarker.getId());
			theMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placeLatLng, 15), 3000, null);
		}
	}
	
	private void removeMarker(Long placeId) {
		String markerId = markerIdsMap.get(placeId);
		if(markerId != null)
			for(Marker marker: placeMarkers) {
				if(markerId.equals(marker.getId())) {
					marker.remove();
					markerIdsMap.remove(placeId);
				}
			}
	}

// LOCATION LISTENER
	@Override
	public void onLocationChanged(Location location) {
		LatLng currLatLng = new LatLng(location.getLatitude(), location.getLongitude());
		theMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currLatLng, 15), 3000, null);
	}

	@Override
	public void onProviderDisabled(String arg0) {
	}

	@Override
	public void onProviderEnabled(String arg0) {
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}

// ASYNC TASK
	private class FilterPlace extends AsyncTask<Integer, Void, Boolean> {
		
		private Place place;
		
		@Override
		protected Boolean doInBackground(Integer... placeNoArr) {
			// firstly check if a place is consistent with a user
			String weatherStr = null;
			
			this.place = detectedPlaces.get(placeNoArr[0].intValue());
			if(!isFilteredSuccessFulByUser(place))
				return false;
			else {
				// rest: check if a place is consistent with current weather
				// get search URL for weather
					
				String weatherSearchStr = prepareWeatherSearchStr(place.getLocation());

				HttpClient httpClient = new DefaultHttpClient();
				try {
					// get response with weather
					weatherStr = getStringFromURL(httpClient, weatherSearchStr);
					if(isFilteredSuccessfulByWeather(place.getKindOfSpace(), weatherStr))
						return true;
				} catch(Exception e){
					e.printStackTrace(); 
				}
			}
			return false;
		}

		private String prepareWeatherSearchStr(LatLng location) {
			String weatherSearchStr = "http://api.openweathermap.org/data/2.5/weather?lat=" + String.valueOf(location.latitude) +
					"&lon=" + String.valueOf(location.longitude);
			return weatherSearchStr;
		}

		private boolean isFilteredSuccessFulByUser(Place place) {
            if(user == null)
            	user = new User();         
     			
     		// check interests
     		if(!areUserInterestsSatisfied(place))
     				return false;
     		
            return true;
		}

		private boolean areUserInterestsSatisfied(Place place) {
			
			List<String> userInterests = user.getInterests();
			// unknown interests => all interests
			if(userInterests == null || userInterests.isEmpty())
				return true;
			
			String[] placeCategories = place.getCategories();
			// unknown interests => all interests
			if(placeCategories == null)
				return true;
			
			for(String interest: userInterests) {
				for(int i=0; i<placeCategories.length; i++) {
	         		if(interest.equals(placeCategories[i])) {
	         			return true;
	         		}
				}
				
         	}
			return false;
		}
		
		private String getStringFromURL(HttpClient httpClient,
				String searchURL) throws ClientProtocolException, IOException {
			StringBuilder URLBuilder = new StringBuilder();
			HttpGet httpGet = new HttpGet(searchURL);
			HttpResponse httpResponse = httpClient.execute(httpGet);
			StatusLine searchStatus = httpResponse.getStatusLine();
			
			if (searchStatus.getStatusCode() == 200) {
				HttpEntity httpEntity = httpResponse.getEntity();
				InputStream content = httpEntity.getContent();
				InputStreamReader input = new InputStreamReader(content);
				BufferedReader reader = new BufferedReader(input);
				
				String lineIn;
				while ((lineIn = reader.readLine()) != null) {
					URLBuilder.append(lineIn);
				}
			}
			return URLBuilder.toString();
		}
		
		//process data retrieved from doInBackground
		protected void onPostExecute(Boolean placeOk) {
			if(placeOk)	
				addMarker(place);				
		}
		
		private boolean isFilteredSuccessfulByWeather(KindOfSpace placeKOS, String weatherResult) throws JSONException {
			WeatherConditions weatherConds = getWeatherCondFromJSON(weatherResult);
			if(weatherConds == null)
				return false;
			
			List<KindOfSpace> kindsOfSpace = Space.getKindsOfSpaceOkForWeather(weatherConds);
			for(KindOfSpace kos: kindsOfSpace) {
				if(kos == placeKOS)
					return true;
			}
			
			return false;
		}

		private WeatherConditions getWeatherCondFromJSON(String weatherCondsStr) throws JSONException {
			if(weatherCondsStr == null)
				return null;
			
			double temp, windStr;
			Boolean precip = false;
			
			JSONObject resultObject = new JSONObject(weatherCondsStr);
			// wiatr 
			JSONObject windObject = resultObject.getJSONObject("wind");
			windStr = windObject.getDouble("speed");
			// temperatura
			JSONObject tempObject = resultObject.getJSONObject("main");
			temp = tempObject.getDouble("temp"); 
			// opady
			JSONArray precipArray = resultObject.getJSONArray("weather");
			for (int p=0; p<precipArray.length(); p++) {
				// parse each weather cond info
				JSONObject weatherCondObject = precipArray.getJSONObject(p);
				int weatherCode = weatherCondObject.getInt("id");

				// maybe the next weather data informs about any precipitation or sth
				if(precip != null && precip == false)
					precip = isPrecipitation(weatherCode);
			}
			
			return new WeatherConditions(temp, windStr, precip);
		}

		private Boolean isPrecipitation(int weatherCode) {
			if(weatherCode >=200 && weatherCode < 400)	// thunderstorm or drizzle
				return true;
			if(weatherCode >=500 && weatherCode < 600)	// rain
				return true;
			// extreme
			switch(weatherCode) {
			case 900:
			case 901:
			case 902:
			case 906:
				return true;
			}
			return false;
		}
	}
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		contextPlaceConnector.removePlaceEventListener(placeEventListener);
	}
}
