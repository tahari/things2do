package pl.edu.agh.gthings2do;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.qualcommlabs.usercontext.Callback;
import com.qualcommlabs.usercontext.ContextInterestsConnector;
import com.qualcommlabs.usercontext.ContextInterestsConnectorFactory;
import com.qualcommlabs.usercontext.protocol.profile.AttributeCategory;
import com.qualcommlabs.usercontext.protocol.profile.Profile;
import com.qualcommlabs.usercontext.protocol.profile.ProfileAttribute;

import pl.edu.agh.gthings2do.model.user.User;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class UserProfileActivity extends Activity {

	private User user = null;
	private TextView[] tvUserInterests;
	
	private boolean noInterests = false;
	
	private ContextInterestsConnector contextInterestsConnector;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);

		setupConnectors();
		getUserProfile();
	}
	
	private void setupConnectors() {
        contextInterestsConnector = ContextInterestsConnectorFactory.get(this);
	}

	private void getUserProfile() {
		contextInterestsConnector.requestProfile(new Callback<Profile>() { 
        	@Override 
        	public void success(Profile profile) { 
        		// do something with profile 
        		parseUserProfile(profile);
        		initView();
        		loadUserProfile();
        	} 
        	@Override 
        	public void failure(int statusCode, String errorMessage) { 
        		// failed with errorCode
        	} 
        }); 
	}
	
	protected void parseUserProfile(Profile profile) {
		user = new User();
		
		if(profile == null)
			return;

		user = new User();
        Map<String, ProfileAttribute> userProfileAttrs = profile.getAttributes();

 		// get interests
        ProfileAttribute userInterests = userProfileAttrs.get("Interests");
        List<AttributeCategory> userInterestsList = userInterests.getAttributeCategories();		
		// unknown interests => all interests
		if(userInterestsList == null || userInterestsList.isEmpty())
			return;
		
		List<String> interestsAsStringList = new LinkedList<String>();
		for(AttributeCategory interest: userInterestsList) {
				// min likelihood is satisfied
         		if(interest.getLikelihood() > SearchPlacesActivity.INTEREST_MIN_LIKELIHOOD) {
         			interestsAsStringList.add(interest.getKey());
         		}			
     	}
		user.setInterests(interestsAsStringList);
	}
	
	private void initView() {
		// interests
		LinearLayout llInterests = (LinearLayout) this.findViewById(R.id.llInterests);

		LayoutParams cbParams = new LayoutParams(
				   LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		int interestsCount;
		if(user.getInterests() != null && user.getInterests().size() > 0)
			interestsCount = user.getInterests().size();
		else {
			interestsCount = 1;
			noInterests = true;
		}
			
		tvUserInterests = new TextView[interestsCount];
		for(int i=0; i<tvUserInterests.length; i++) {
			tvUserInterests[i] = new TextView(this);
			tvUserInterests[i].setLayoutParams(cbParams);
			llInterests.addView(tvUserInterests[i]);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadUserProfile();
	}

	private void loadUserProfile() {	
		if(user != null)
			if(!noInterests) {
				int i = 0;
				for(String interest: user.getInterests()) {
					tvUserInterests[i].setText(interest);
					i++;
				}
			} else {
				tvUserInterests[0].setText("Brak wyróżnionych zainteresowań.");
			}
	}
}
