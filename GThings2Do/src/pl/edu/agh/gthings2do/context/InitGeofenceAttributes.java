package pl.edu.agh.gthings2do.context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import pl.edu.agh.gthings2do.R;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class InitGeofenceAttributes {

	private static final String AUTHORIZATION_TOKEN = "f9390ec25f9bdd279e96ece8ffd182b5";
	// Park Wyspianskiego, biblioteka, Azory Ognisko, sklep spo�ywczy, biuro rachunkowe
	private static int[] PLACES_IDS = { 274071, 274072, 274073, 274074, 274076, 274402, 3100793, 3100803 };
	private String[] placesJsons;
	
	public void initAllPlaces(Context ctx) {
		placesJsons = ctx.getResources().getStringArray(R.array.update_jsons);
		int placesCount = PLACES_IDS.length;
		for(int i=0; i<placesCount; i++) {
			updatePlace(i);
		}
		
	}

	private void updatePlace(int i) {
		
		String searchURL = "https://manager.gimbal.com/api/geofences/";
		String fullSearchURL = searchURL + PLACES_IDS[i];
		new UpdatePlaces().execute(fullSearchURL, placesJsons[i]);	
	}
	
	private class UpdatePlaces extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... searchData) {
			HttpClient httpClient = new DefaultHttpClient();
			StringBuilder URLBuilder = new StringBuilder();
			
			//HTTP Put receives URL string
			HttpPut httpPut = new HttpPut(searchData[0]);
			try {
				httpPut.setEntity(new StringEntity(searchData[1]));
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
			httpPut.setHeader("AUTHORIZATION", "Token token=" + AUTHORIZATION_TOKEN);
			httpPut.setHeader("Content-Type", "application/json");
			//execute PUT with Client - return response
			HttpResponse httpResponse;
			try {
				httpResponse = httpClient.execute(httpPut);
				//check response status
				StatusLine searchStatus = httpResponse.getStatusLine();
				
				//only carry on if response is OK
				if (searchStatus.getStatusCode() == 200) {
					//put response
					HttpEntity httpEntity = httpResponse.getEntity();
					InputStream content = httpEntity.getContent();
					InputStreamReader input = new InputStreamReader(content);
					BufferedReader reader = new BufferedReader(input);
					//read a line at a time, append to string builder
					String lineIn;
					while ((lineIn = reader.readLine()) != null) {
						URLBuilder.append(lineIn);
					}
				}
				Log.d("request", URLBuilder.toString());
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	  }
}
