package pl.edu.agh.gthings2do;

import pl.edu.agh.gthings2do.context.InitGeofenceAttributes;

import com.qualcommlabs.usercontext.Callback;
import com.qualcommlabs.usercontext.ContextCoreConnector;
import com.qualcommlabs.usercontext.ContextCoreConnectorFactory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

	private ContextCoreConnector contextCoreConnector;

        
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// UNCOMMENT to init place attributes for places defined in Gimbal (category, kind of space, target gender, target age ranges)
		//InitGeofenceAttributes init = new InitGeofenceAttributes();
		//init.initAllPlaces(getApplicationContext());
		
		contextCoreConnector = ContextCoreConnectorFactory.get(this);
        checkContextConnectorStatus();

	}

	private void checkContextConnectorStatus() {
        if (contextCoreConnector.isPermissionEnabled()) {
            // Gimbal is already enabled
        }
        else {
             contextCoreConnector.enable(this, new Callback<Void>() {

                @Override
                public void success(Void arg0) {
                    // Gimbal is ready
                }

                @Override
                public void failure(int arg0, String arg1) {
                    Log.e("failed to enable", arg1);
                }
            });
        }
    }

	public void searchPlaces(View v) {
		Intent intent = new Intent(this, SearchPlacesActivity.class);
		startActivity(intent);
	}

	public void showUserProfile(View v) {
		Intent intent = new Intent(this, UserProfileActivity.class);
		startActivity(intent);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		contextCoreConnector.setCurrentActivity(null);
	}

	@Override
	protected void onResume() {
		super.onResume();
		contextCoreConnector.setCurrentActivity(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
